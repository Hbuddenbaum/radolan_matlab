function RA = read_radolan(file)
%Radolan selber einlesen
% RA = read_radolan(file);
% Returns structure RA
% RA.DAT is rain data in mm, Missig = 250;
% RA.DATn is rain data in mm, Missig = NaN;

fid = fopen(file);% fopen([p f]);
    RA.SF            = fread(fid,2,'*char')';
    RA.day           = fread(fid,2,'*char')';
    RA.hour          = fread(fid,2,'*char')';
    RA.min           = fread(fid,2,'*char')';
    RA.radarstandort = fread(fid,5,'*char')'; % für Komposit wird immer 10000 verwendet
    RA.month         = fread(fid,2,'*char')';
    RA.year          = fread(fid,2,'*char')';
    RA.BY            = fread(fid,2,'*char')'; % BY
    RA.produktlaenge = fread(fid,7,'*char')';
    RA.VS            = fread(fid,2,'*char')';
    RA.formatversion = fread(fid,2,'*char')'; % 3: 150 km Radius (ab 30.06.2010 gesetzt)
    RA.SW            = fread(fid,2,'*char')';
    RA.softw_version = fread(fid,9,'*char')';
    RA.PR            = fread(fid,2,'*char')';
                       fread(fid,1,'*char'); % Empty space
    RA.genauigkeit   = fread(fid,4,'*char')'; %  „ E-00“ für ganze Zahlen, „ E-01“ für 1/10; „ E-02“ für 1/100
    RA.INT           = fread(fid,3,'*char')';
    RA.intervalldauer= fread(fid,4,'*char')'; % Intervalldauer in Minuten; bei den Summenprodukten W1, W2, W3 und W4 ist die Angabe mit zehn zu multplizieren, um auf die Minuten zu kommen 
    RA.GP            = fread(fid,2,'*char')';
    if ~strcmp(RA.GP, 'GP')
        disp(['Something is wrong with GP in ' file]);
        RA.GP            = fread(fid,2,'*char')';
    end
    RA.anzahlpixel   = fread(fid,9,'*char')'; 
        RA.ze = str2double(RA.anzahlpixel(1:4));
        RA.sp = str2double(RA.anzahlpixel(6:9));
    % RA.VV            = fread(fid,2,'*char')'; % Nur RADVOR-Produkte RV, RS, RQ und RE
    % RA.vzeitpunkt    = fread(fid,4,'*char')'; % Nur RADVOR-Produkte RV, RS, RQ und RE
    % RA.MF            = fread(fid,2,'*char')'; % Nur RADVOR-Produkte RV, RS, RQ und RE
    % RA.dezimalwert   = fread(fid,9,'*char')'; % Nur RADVOR-Produkte RV, RS, RQ und RE
    % RA.QN            = fread(fid,2,'*char')'; % Nur RADVOR-Produkte RQ und RE
    % RA.quantif       = fread(fid,4,'*char')'; % Nur RADVOR-Produkte RQ und RE
    RA.MS            = fread(fid,2,'*char')';
    RA.tlMS          = fread(fid,3,'*char')'; RA.tlMS = str2num(RA.tlMS);% Textlänge
    RA.textMS        = fread(fid,RA.tlMS,'*char')';
    RA.ST            = fread(fid,2,'*char')';
    RA.tlST          = fread(fid,3,'*char')'; RA.tlST = str2num(RA.tlST);% Textlänge
    RA.textST        = fread(fid,RA.tlST,'*char')';
    RA.ETX           = fread(fid,1,'*char')';
    RA.dat           = fread(fid,[RA.sp RA.ze],'*uint16'); % Maybe sp first?
fclose(fid);

%% Daten umwandeln
RA.faktor = str2double(['1' RA.genauigkeit]);

[ze, sp] = size(RA.dat);
RA.DAT = zeros(ze, sp); RA.B13 = zeros(ze, sp);

for b=1:sp % Schleife nur über die Spalten
    bin16 = dec2bin(RA.dat(:,b),16);
    RA.DAT(:,b) = bin2dec(bin16(:,5:16)) .* RA.faktor;
    RA.B13(:,b) = bin2dec(bin16(:,1));
    RA.B14(:,b) = bin2dec(bin16(:,2));
    RA.B15(:,b) = bin2dec(bin16(:,3));
    RA.B16(:,b) = bin2dec(bin16(:,4));
end

RA.DATn = RA.DAT;
if ze == 900
    RA.DATn(logical(RA.B15))=NaN;
elseif ze == 1100
    RA.DATn(logical(RA.B13))=NaN;
end

% subplot(4,2,1:4),imagesc(RA.DATn), axis equal tight; colorbar
% subplot(4,2,5),imagesc(RA.B13), axis equal tight off; title('B13'); colorbar
% subplot(4,2,6),imagesc(RA.B14), axis equal tight off; title('B14'); colorbar
% subplot(4,2,7),imagesc(RA.B15), axis equal tight off; title('B15'); colorbar
% subplot(4,2,8),imagesc(RA.B16), axis equal tight off; title('B16'); colorbar